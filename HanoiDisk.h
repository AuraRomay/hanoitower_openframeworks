#pragma once
#include "ofRectangle.h"
#include "ofPoint.h"
#include "ofxAnimatableOfPoint.h"
#include "ofxAnimatable.h"
#include "ofEvents.h"

enum {ANI_STEP1 = 1, ANI_STEP2, ANI_STEP3};

class HanoiDisk {
public:
	ofPoint targetPoint;
	ofxAnimatableOfPoint ani;
	ofRectangle rect;
	int pegHeight;
	int aniCurrentStep;
	int color[3];
	ofEvent<float> aniEnd;

	void init(float x, float y, float w, float h);
	void update();
	void draw();
	void moveTo(float x, float y);
	void aniStepFinished(ofxAnimatable::AnimationEvent &a);
};


