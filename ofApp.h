#pragma once

#include "ofMain.h"
#include "HanoiDisk.h"
#include <vector>
#include <list>

using namespace std;

struct HanoiMove {
	HanoiDisk *disk;
	ofPoint targetPoint;
};

struct HanoiPeg {
	vector<HanoiDisk*> stack;
	ofPoint pos;
};

class ofApp : public ofBaseApp{

	public:

		int loot;
		HanoiDisk *diskArray;
		
		HanoiPeg peg1;
		HanoiPeg peg2;
		HanoiPeg peg3;

		ofVec2f pegPos;
		int pegOffset;
		int diskWidth;
		int diskHeight;
		int diskHalfHeight;

		list<HanoiMove> moves;


		void playNextMove();
		void calculateHanoiMoves(int diskNum, HanoiPeg &beg, HanoiPeg &aux, HanoiPeg &end);
		void moveFinished(float& r);
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
};
