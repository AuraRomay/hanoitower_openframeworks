#include "HanoiDisk.h"
#include "ofxAnimatable.h"

void HanoiDisk::init(float x, float y, float w, float h)
{
	rect.set(x, y, w, h);
	pegHeight = 200;
	color[0] = ofRandom(0, 255);
	color[1] = ofRandom(0, 255);
	color[2] = ofRandom(0, 255);
	ani.setPosition(rect.getPosition());

	//set repeating style
	ani.setRepeatType(PLAY_ONCE);

	//set animation curve
	ani.setCurve(EASE_IN_EASE_OUT);
	ani.setDuration(1);
	ofAddListener(ani.animFinished, this, &HanoiDisk::aniStepFinished);
}

void HanoiDisk::update()
{
	ani.update(1.0f / 30.0f);
	rect.setPosition(ani.getCurrentPosition());
}

void HanoiDisk::draw()
{
	//ofSetBackgroundColor(ofRandom(0, 255));
	ofSetColor(0);
	ofNoFill();
	ofDrawRectRounded(rect, 10);
	ofSetColor(color[0], color[1], color[2]);
	ofFill();
	ofDrawRectRounded(rect, 10);
}

void HanoiDisk::moveTo(float x, float y)
{
	targetPoint.set(x,y);
	aniCurrentStep = ANI_STEP1;
	ofPoint p(rect.x, pegHeight);
	ani.animateTo(p);
}

void HanoiDisk::aniStepFinished(ofxAnimatable::AnimationEvent &e)
{
	if (aniCurrentStep == ANI_STEP1) {
		ofPoint p(targetPoint.x,rect.y);
		ani.animateTo(p);
	} else if (aniCurrentStep == ANI_STEP2) {
		ofPoint p(targetPoint.x, targetPoint.y);
		ani.animateTo(p);
	} else if (aniCurrentStep == ANI_STEP3) {
		ofLog(OF_LOG_NOTICE, "Animation end");
		float r = 10.0f;
		ofNotifyEvent(aniEnd, r);
	}
	aniCurrentStep++;
}
