#include "ofApp.h"



void ofApp::playNextMove()
{
	list<HanoiMove>::iterator curretMove = moves.begin();

	if (curretMove != moves.end()) {
		HanoiDisk *disk = (*curretMove).disk;
		ofPoint target = (*curretMove).targetPoint;
		disk->moveTo(target.x, target.y);
		moves.pop_front();
	}
}



void ofApp::calculateHanoiMoves(int diskNum, HanoiPeg &beg, HanoiPeg &aux, HanoiPeg &end)
{
	if (diskNum == 1) {
		HanoiMove move;
		move.disk = beg.stack.back();
		int posy = pegPos.y - end.stack.size()*diskHeight;
		move.targetPoint.set(end.pos.x, posy);
		moves.push_back(move);
		end.stack.push_back(beg.stack.back());
		beg.stack.pop_back();
	}
	else {
		calculateHanoiMoves(diskNum - 1, beg, end, aux);
		calculateHanoiMoves(1, beg, aux, end);
		calculateHanoiMoves(diskNum - 1, aux, beg, end);
	}
}

void ofApp::moveFinished(float &e)
{
	playNextMove();
}

//--------------------------------------------------------------
void ofApp::setup(){
	cout << "Pon loot" << endl;
	cin >> loot;
	diskArray = new HanoiDisk[loot];
	ofSetFrameRate(30);
	ofSetRectMode(OF_RECTMODE_CENTER);


	pegPos.set(200, 500);
	pegOffset = 300;
	diskWidth = 200;
	diskHeight = 30;
	diskHalfHeight = 15;
	int disksH = loot - 1;
	for (int i = 0; i < loot; i++) {
		diskArray[i].init(pegPos.x, pegPos.y - ((diskHeight /*+ diskHalfHeight*/)*i), diskWidth + (disksH*30), diskHeight);
		disksH--;
		peg1.stack.push_back(&diskArray[i]);
	}/*
	diskArray[0].init(pegPos.x, pegPos.y - diskHalfHeight, diskWidth+80, diskHeight);
	peg1.stack.push_back(&diskArray[0]);
	diskArray[1].init(pegPos.x, pegPos.y - (diskHeight+diskHalfHeight), diskWidth + 40, diskHeight);
	peg1.stack.push_back(&diskArray[1]);
	diskArray[2].init(pegPos.x, pegPos.y - (diskHeight*2+diskHalfHeight), diskWidth, diskHeight);
	peg1.stack.push_back(&diskArray[2]);*/
	

	peg1.pos = pegPos;
	peg2.pos.set(pegPos.x + pegOffset, pegPos.y);
	peg3.pos.set(pegPos.x + pegOffset*2, pegPos.y);
	calculateHanoiMoves(loot, peg1, peg2, peg3);
	cout << moves.size() << endl;
	playNextMove();
	for (int i = 0; i < loot; i++) {
		ofAddListener(diskArray[i].aniEnd, this, &ofApp::moveFinished);
	}
	/*ofAddListener(diskArray[0].aniEnd, this, &ofApp::moveFinished);
	ofAddListener(diskArray[1].aniEnd, this, &ofApp::moveFinished);
	ofAddListener(diskArray[2].aniEnd, this, &ofApp::moveFinished);*/


}

//--------------------------------------------------------------
void ofApp::update(){
	for (int i = 0; i < loot; i++) {
		diskArray[i].update();
	}
}

//--------------------------------------------------------------
void ofApp::draw(){

	ofSetColor(120, 120, 0);
	ofDrawRectangle(peg1.pos, 5, 700);
	ofDrawRectangle(peg2.pos, 5, 700);
	ofDrawRectangle(peg3.pos, 5, 700);
	
	for (int i = 0; i < loot; i++) {
		diskArray[i].draw();
	}
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}



//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
